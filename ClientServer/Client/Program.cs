﻿using Client;
using SharedModel;
using System;
using System.Linq;
using System.Threading.Tasks;

public class Program
{
    static void Main(string[] args)
    {
        var apiTask = new ApiClient<SharedModel.Task>();
        var apiPeople = new ApiClient<People>();

    MainMenu:
        while (true)
        {
            Console.WriteLine("Select an option:");
            Console.WriteLine("1. Task");
            Console.WriteLine("2. People");
            Console.WriteLine("3. Exit");

            var choice = Console.ReadLine();

            switch (choice)
            {
                case "1":
                    while (true)
                    {
                        Console.WriteLine("1. Create Task");
                        Console.WriteLine("2. View Task");
                        Console.WriteLine("3. View All Tasks");
                        Console.WriteLine("4. View Tasks for Today");
                        Console.WriteLine("5. View Tasks Sorted by Date");
                        Console.WriteLine("6. Delete Task");
                        Console.WriteLine("7. Back to Main Menu");
                        Console.WriteLine("8. Exit");

                        var choice_1 = Console.ReadLine();

                        switch (choice_1)
                        {
                            case "1":
                                CreateTask(apiTask);
                                break;
                            case "2":
                                ViewTask(apiTask);
                                break;
                            case "3":
                                ViewAllTasks(apiTask);
                                break;
                            case "4":
                                ViewTasksToday(apiTask);
                                break;
                            case "5":
                                ViewTasksByDate(apiTask);
                                break;
                            case "6":
                                DeleteTask(apiTask);
                                break;
                            case "7":
                                goto MainMenu;
                            case "8":
                                return;
                            default:
                                Console.WriteLine("Invalid option. Please try again.");
                                break;
                        }
                    }

                case "2":
                    while (true)
                    {
                        Console.WriteLine("1. Create Person");
                        Console.WriteLine("2. View Person");
                        Console.WriteLine("3. View Person Sorted by LastName");
                        Console.WriteLine("4. Delete Person");
                        Console.WriteLine("5. Back to Main Menu");
                        Console.WriteLine("6. Exit");

                        var choice_2 = Console.ReadLine();

                        switch (choice_2)
                        {
                            case "1":
                                CreatePerson(apiPeople);
                                break;
                            case "2":
                                ViewPerson(apiPeople);
                                break;
                            case "3":
                                ViewPeopleByLastname(apiPeople);
                                break;
                            case "4":
                                DeletePerson(apiPeople);
                                break;
                            case "5":
                                goto MainMenu;
                            case "6":
                                return;
                            default:
                                Console.WriteLine("Invalid option. Please try again.");
                                break;
                        }
                    }
                case "3":
                    return;
                default:
                    Console.WriteLine("Invalid option. Please try again.");
                    break;
            }
        }
    }
    #region Методы
    /// <summary>
    /// Метод для создания задачи.
    /// </summary>
    /// <param name="apiTask">Клиент API для работы с задачами.</param>
    static void CreateTask(ApiClient<SharedModel.Task> apiTask)
    {
        Console.WriteLine("Enter task details:");
        Console.Write("Title: ");
        string title = Console.ReadLine();
        Console.Write("Description: ");
        string description = Console.ReadLine();
        Console.Write("Deadline (dd.MM.yyyy HH:mm): ");
        if (DateTime.TryParseExact(Console.ReadLine(), "dd.MM.yyyy HH:mm", null, System.Globalization.DateTimeStyles.None, out DateTime deadline))
        {
            Console.Write("Assign task to person (enter person ID): ");
            if (int.TryParse(Console.ReadLine(), out int personId))
            {
                if (deadline > DateTime.Now)
                {
                    var newTask = new SharedModel.Task()
                    {
                        Title = title,
                        Description = description,
                        Deadline = deadline,
                        PeopleId = personId
                    };

                    var result = apiTask.CreateAsync(newTask).GetAwaiter().GetResult();

                    if (result != null)
                    {
                        Console.WriteLine("Task created successfully.");
                        Console.WriteLine($"Task ID: {result.TaskId}");
                    }
                    else
                    {
                        Console.WriteLine("Failed to create the task.");
                    }
                }
                else
                {
                    Console.WriteLine("Deadline should be greater than the current date.");
                }
            }
            else
            {
                Console.WriteLine("Invalid person ID format.");
            }
        }
        else
        {
            Console.WriteLine("Invalid date format. Use the format dd.MM.yyyy HH:mm.");
        }
    }
    /// <summary>
    /// Метод для просмотра задачи по id.
    /// </summary>
    /// <param name="apiTask">Клиент API для работы с задачами.</param>
    static void ViewTask(ApiClient<SharedModel.Task> apiTask)
    {
        Console.Write("Enter task ID to view: ");
        if (int.TryParse(Console.ReadLine(), out int taskId))
        {
            var result = apiTask.GetAsync(taskId.ToString()).GetAwaiter().GetResult();

            if (result != null)
            {
                Console.WriteLine("Task Details:");
                Console.WriteLine("{0,-10} {1,-20} {2,-20} {3,-20}", "Task ID", "Title", "Description", "Deadline");
                Console.WriteLine($"{result.TaskId,-10} {result.Title,-20} {result.Description,-20} {result.Deadline.Value.ToString("dd.MM.yyyy HH:mm"),-20}");
            }
            else
            {
                Console.WriteLine("Task not found.");
            }
        }
        else
        {
            Console.WriteLine("Invalid task ID format.");
        }
    }
    /// <summary>
    /// Метод для просмотра всех задач.
    /// </summary>
    /// <param name="apiTask">Клиент API для работы с задачами.</param>
    static void ViewAllTasks(ApiClient<SharedModel.Task> apiTask)
    {
        var tasks = apiTask.GetAllAsync().GetAwaiter().GetResult();

        if (tasks != null && tasks.Any())
        {
            Console.WriteLine("List of Tasks:");
            Console.WriteLine("{0,-10} {1,-20} {2,-20} {3,-20}", "Task ID", "Title", "Description", "Deadline");
            foreach (var task in tasks)
            {
                Console.WriteLine($"{task.TaskId,-10} {task.Title,-20} {task.Description,-20} {task.Deadline.Value.ToString("dd.MM.yyyy HH:mm"),-20}");
            }
        }
        else
        {
            Console.WriteLine("No tasks found.");
        }
    }

    /// <summary>
    /// Метод для просмотра задач, запланированных на сегодня.
    /// </summary>
    /// <param name="apiTask">Клиент API для работы с задачами.</param>
    static void ViewTasksToday(ApiClient<SharedModel.Task> apiTask)
    {
        var tasks = apiTask.GetTasksToday().GetAwaiter().GetResult();

        if (tasks != null && tasks.Count > 0) // Проверяем длину коллекции
        {
            Console.WriteLine("Tasks for Today:");
            Console.WriteLine("{0,-10} {1,-20} {2,-20} {3,-20}", "Task ID", "Title", "Description", "Deadline");
            foreach (var task in tasks)
            {
                Console.WriteLine($"{task.TaskId,-10} {task.Title,-20} {task.Description,-20} {task.Deadline.Value.ToString("dd.MM.yyyy HH:mm"),-20}");
            }
        }
        else
        {
            Console.WriteLine("No tasks for today.");
        }
    }
    /// <summary>
    /// Метод для просмотра задач, отсортированных по дате.
    /// </summary>
    /// <param name="apiTask">Клиент API для работы с задачами.</param>
    static void ViewTasksByDate(ApiClient<SharedModel.Task> apiTask)
    {
        var tasks = apiTask.GetTasksSortedByDate().GetAwaiter().GetResult();

        if (tasks != null && tasks.Count > 0) // Проверяем длину коллекции
        {
            Console.WriteLine("Tasks Sorted by Date:");
            Console.WriteLine("{0,-10} {1,-20} {2,-20} {3,-20}", "Task ID", "Title", "Description", "Deadline");
            foreach (var task in tasks)
            {
                Console.WriteLine($"{task.TaskId,-10} {task.Title,-20} {task.Description,-20} {task.Deadline.Value.ToString("dd.MM.yyyy HH:mm"),-20}");
            }
        }
        else
        {
            Console.WriteLine("No tasks found.");
        }
    }


    /// <summary>
    /// Метод для удаления задачи по id.
    /// </summary>
    /// <param name="apiTask">Клиент API для работы с задачами.</param>
    static void DeleteTask(ApiClient<SharedModel.Task> apiTask)
    {
        Console.Write("Enter task ID to delete: ");
        if (int.TryParse(Console.ReadLine(), out int taskId))
        {
            var result = apiTask.DeleteAsync(taskId.ToString()).GetAwaiter().GetResult();

            if (result)
            {
                Console.WriteLine("Task deleted successfully.");
            }
            else
            {
                Console.WriteLine("Task not found or failed to delete.");
            }
        }
        else
        {
            Console.WriteLine("Invalid task ID format.");
        }
    }
    /// <summary>
    /// Метод для добавления человека.
    /// </summary>
    /// <param name="apiPeople">Клиент API для работы с людьми.</param>
    static void CreatePerson(ApiClient<People> apiPeople)
    {
        Console.WriteLine("Enter person details:");
        Console.Write("First Name: ");
        string firstName = Console.ReadLine();
        Console.Write("Last Name: ");
        string lastName = Console.ReadLine();
        Console.Write("Job Title: ");
        string jobTitle = Console.ReadLine();

        var newPerson = new People()
        {
            FirstName = firstName,
            LastName = lastName,
            JobTitle = jobTitle
        };

        var result = apiPeople.CreateAsync(newPerson).GetAwaiter().GetResult();

        if (result != null)
        {
            Console.WriteLine("Person created successfully.");
            Console.WriteLine($"Person ID: {result.PeopleId}");
        }
        else
        {
            Console.WriteLine("Failed to create the person.");
        }
    }
    /// <summary>
    /// Метод для просмотра информации о человеке по id.
    /// </summary>
    /// <param name="apiPeople">Клиент API для работы с людьми.</param>
    static void ViewPerson(ApiClient<People> apiPeople)
    {
        Console.Write("Enter person ID to view: ");
        if (int.TryParse(Console.ReadLine(), out int personId))
        {
            var result = apiPeople.GetAsync(personId.ToString()).GetAwaiter().GetResult();

            if (result != null)
            {
                Console.WriteLine("Person Details:");
                Console.WriteLine("{0,-10} {1,-20} {2,-20} {3,-20}", "Person ID", "First Name", "Last Name", "Job Title");
                Console.WriteLine("{0,-10} {1,-20} {2,-20} {3,-20}", result.PeopleId, result.FirstName, result.LastName, result.JobTitle);
            }
            else
            {
                Console.WriteLine("Person not found.");
            }
        }
        else
        {
            Console.WriteLine("Invalid person ID format.");
        }
    }
    /// <summary>
    /// Метод для просмотра людей, отсортированных по Фамилии.
    /// </summary>
    /// <param name="apiPeople">Клиент API для работы с людьм.</param>
    static void ViewPeopleByLastname(ApiClient<People> apiPeople)
    {
        var results = apiPeople.GetPeopleSortedByLastName().GetAwaiter().GetResult();

        if (results != null && results.Count > 0)
        {
            Console.WriteLine("People Sorted by Last Name:");
            Console.WriteLine("{0,-10} {1,-20} {2,-20} {3,-20}", "People ID", "First Name", "Last Name", "Job Title");
            foreach (var result in results)
            {
                Console.WriteLine("{0,-10} {1,-20} {2,-20} {3,-20}", result.PeopleId, result.FirstName, result.LastName, result.JobTitle);
            }
        }
        else
        {
            Console.WriteLine("No people found.");
        }
    }

    /// <summary>
    /// Метод для удаления человека.
    /// </summary>
    /// <param name="apiPeople">Клиент API для работы с людьми.</param>
    static void DeletePerson(ApiClient<People> apiPeople)
    {
        Console.Write("Enter person ID to delete: ");
        if (int.TryParse(Console.ReadLine(), out int personId))
        {
            var result = apiPeople.DeleteAsync(personId.ToString()).GetAwaiter().GetResult();

            if (result)
            {
                Console.WriteLine("Person deleted successfully.");
            }
            else
            {
                Console.WriteLine("Failed to delete the person.");
            }
        }
        else
        {
            Console.WriteLine("Invalid person ID format.");
        }
    }
    #endregion
}
